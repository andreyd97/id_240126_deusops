## Clone project and launch Vagrant VM (VirtualBox)


```
git clone https://gitlab.com/andreyd97/id_240126_deusops.git
cd id_240126_deusops
vagrant up
```

## Vagrantfile
```

$nginxnode = <<-SCRIPT
apt-get update // Обновляем версии пакетов локальных репозиториев
apt-get upgrade -y //Обновляем все пакеты до последней доступной версии в репозиториях
apt-get install nginx -y //установка Nginx
curl -sL https://deb.nodesource.com/setup_16.x | sudo -E bash - //ставим nodejs через архив персональных пакетов PPA
apt-get install -y nodejs //устанавливаем сам nginx
SCRIPT

$game_run = <<-SCRIPT
sudo cp nginx.conf /etc/nginx/nginx.conf //копируем файл вместо оригинального nginx.conf
sudo cp -r 2048-game/ /var/www/html/ //копируем директорию с игрой в /var/www/html/
cd /var/www/html/2048-game //переходим в папку с игрой
sudo npm install --include=dev //ставим нужные пакеты
sudo npm run build //билдим приложение
sudo pkill -f nginx & wait $! //останавливаем все текущие процессы nginx
sudo service nginx start //запускаем nginx
SCRIPT

$unit = <<-SCRIPT
sudo cp testnode.service /etc/systemd/system/ //копируем файл в целевую директорию
sudo systemctl daemon-reload //перезагружаем демон sytemd
sudo systemctl enable testnode //включаем наш сервис в автозагрузку при включении сервера
sudo systemctl start testnode //запускаем наш сервис
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.hostname = "game01" //устанавливаем имя VM
  config.vm.box = "ubuntu/focal64" //образ VM
  config.vm.network "forwarded_port", guest: 80, host: 9090 //настраиваем проброс портов
  config.vm.provision "shell", inline: $nginxnode //выполняется скрипт (см. выше)
  config.vm.provision "file", source: "./nginx.conf", destination: "./" //копируем файл nginx.conf на VM
  config.vm.provision "file", source: "./2048-game", destination: "./" //копируем директорию с игрой на VM
  config.vm.provision "shell", inline: $game_run //запускаем скрипт билда проекта игры
  config.vm.provision "file", source: "./testnode.service", destination: "./" //копируем unit файл на VM
  config.vm.provision "shell", inline: $unit //Запускаем службу System Unit
end
```
